EXECS=hello-mpi send_recv ping_pong ring pi
MPICC?=mpicc

all: ${EXECS}

hello-mpi: hello-mpi.c
	${MPICC} -o hello-mpi hello-mpi.c

send_recv: send_recv.c
	${MPICC} -o send_recv send_recv.c

ping_pong: ping_pong.c
	${MPICC} -o ping_pong ping_pong.c

ring: ring.c
	${MPICC} -o ring ring.c
pi: pi.c
	${MPICC} -o pi pi.c

clean:
	rm -f ${EXECS}



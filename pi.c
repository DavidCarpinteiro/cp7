#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <stdint.h>
#include <memory.h>
#include <mpi.h>

double rand_double(unsigned int* seed) {
    double value = rand() / (double) RAND_MAX;
    return value;
}

int main(int argc, char** argv) {
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);
    // Find out rank, size
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    int sims[];
    int total = 0;
    
    if (world_rank == 0) {
        //Master
    	int n_sim = 10000000;
        int n_sim_per = n_sim / (world_size-1);
        int left_over = n_sim % (world_size-1);
        sims = int[world_size-1];

        for(int i = 1; i < world_size; i++) {
            if (i == world_size - 1)
                n_sim_per += left_over;
            sims[i-1] = n_sim_per;
        }

        MPI_Bcast(&sims, world_size-1, MPI_INT, 0, MPI_COMM_WORLD);

        MPI_Reduce(&points_in_circle, &total, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

        double pi_estimation = (total / (double) n_sim) * 4.0;
    	printf("Pi estimation: %lf\n", pi_estimation);
    }
    else
    {
        MPI_Bcast(&sims, world_size-1, MPI_INT, i, 0, MPI_COMM_WORLD);

        int n_simulations = sims[world_rank-1];

        unsigned int curr_time = (unsigned int)time(NULL);
        unsigned int seed = curr_time ^ world_rank;
        srand(seed);

        int points_in_circle = 0;
        double x, y;

        for (int i = 0; i < n_simulations; i++) {
            x = rand_double(seed);
            y = rand_double(seed);

            if ((x * x + y * y) <= 1.0)
                points_in_circle++;
        }

        MPI_Reduce(&points_in_circle, &total, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
        printf("Process %d calculated %d points inside circle\n", world_rank, points_in_circle);
    }

    


    MPI_Finalize();
}



